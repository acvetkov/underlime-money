**Статистика платежей Underlime**

Проект создан на Django 1.5.5. Для Django необходимы следующие модули:

- MySQL-Python
- PIL
- South
- django-admin-tools
- django-tinymce
- django-tinymcewrapper
- django-grappelli

Для сборки фронтэнда необходимо установить менеджер пакетов [Node.js](https://www.npmjs.org/)

*Необходимые зависимости:*

- Менеджер пакетов [bower](http://bower.io/)
- JavaScript Task Runner [GruntJS](http://gruntjs.com/)

Все остальные зависимости устанавливаются через bower из конфига /underlime-money/money/static/js/bower.json
В данной директории необходимо выполнить команду
`bower install`

Конфиги сборки приложения для GruntJS (Gruntfile.js, package.json) находятся в корне проекта.
Для работы необходимо выполнить команду `npm install` в корне проекта для установки необходимых плагинов.