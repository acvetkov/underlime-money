from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from money.views import Index

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'underlime.views.home', name='home'),
    # url(r'^underlime/', include('underlime.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^(index\.html)?$', Index.as_view(), name='index'),
)
