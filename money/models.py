# coding=utf-8

import datetime
from django.db import models


class Platforms(models.Model):
    code = models.CharField(
        verbose_name=u'Код',
        max_length=8,
        blank=False,
        unique=True,
    )
    name = models.CharField(
        verbose_name=u'Название',
        max_length=32,
        blank=False,
    )

    pay_k = models.FloatField(
        verbose_name=u'коэффициент валюты',
        default=1
    )

    def as_json(self):
        return dict(
            id=self.id,
            code=self.code,
            name = self.name
        )

    def __unicode__(self):
        return self.name

    class Meta(object):
        verbose_name = u'Плтаформа'
        verbose_name_plural = u'Платформы'


class Projects(models.Model):
    platforms = models.ManyToManyField(
        Platforms,
        verbose_name=u'Список платформ'
    )
    code = models.CharField(
        verbose_name=u'Код',
        max_length=8,
        blank=False,
        unique=True,
    )
    name = models.CharField(
        verbose_name=u'Название',
        max_length=32,
        blank=False,
    )

    def as_json(self):
        return dict(
            id=self.id,
            code=self.code,
            name = self.name
        )

    def __unicode__(self):
        return self.name

    class Meta(object):
        verbose_name = u'Проект'
        verbose_name_plural = u'Проекты'


class Payment(models.Model):
    sum = models.FloatField(
        blank=False,
        verbose_name=u'Сумма платежа'
    )
    platform = models.ForeignKey(
        Platforms,
        verbose_name=u'Платформа',
    )
    project = models.ForeignKey(
        Projects,
        verbose_name=u'Проект'
    )
    date = models.DateTimeField(
        verbose_name=u'Дата платежа',
        default=datetime.datetime.today()
    )

    def __unicode__(self):
        return unicode(self.sum)

    class Meta(object):
        verbose_name = u'Платеж'
        verbose_name_plural = u'Платежи'




