# coding=utf-8
import datetime
import json
import random
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Sum, Avg
from django.http import HttpResponse
from django.views.generic import TemplateView, View
from money.helpers import get_random_list_element
from money.models import Projects, Payment, Platforms


class AppView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super(TemplateView, self).get_context_data(**kwargs)
        context['title'] = "Статистика платежей Underlime"
        return context


class Index(AppView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        context['index_collection'] = json.dumps(self.get_index_collection())
        context['platforms'] = json.dumps([pl.as_json() for pl in Platforms.objects.filter()])
        context['projects'] = json.dumps([pr.as_json() for pr in Projects.objects.filter()])
        return context

    def get_index_collection(self):
        collection = []
        for project in Projects.objects.all():
            for platform in Platforms.objects.all():
                data = {'platform_id': platform.id, 'project_id': project.id}

                total_pay = Payment.objects \
                    .filter(project_id=project.id, platform_id=platform.id) \
                    .aggregate(total_sum=Sum('sum'))

                avg_pay = Payment.objects.filter(project_id=project.id, platform_id=platform.id).aggregate(
                    avg_sum=Avg('sum'))

                data['total_count'] = Payment.objects \
                    .filter(project_id=project.id, platform_id=platform.id) \
                    .count()

                data.update(total_pay)
                data.update(avg_pay)
                collection.append(data)
        return collection
