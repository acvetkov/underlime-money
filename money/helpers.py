import random


def get_random_list_element(list):
    size = list.__len__()
    index = random.randrange(0, size - 1)
    return list[index]
