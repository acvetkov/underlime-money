/*global Backbone, _, IndexPie*/
var IndexView;

(function(){
    'use strict';
    IndexView = Backbone.View.extend({
        el : "#site-content",
        template : $("#index-page-wrapper").html(),
        render : function() {
            this.$el.html(_.template(this.template));
            this.platformsView = new PlatformIndexView({
               collection: this.collection
            });
            this.projectsView = new ProjectIndexView({
                collection: this.collection
            });
        }
    });

    var TablePlatformView = Backbone.View.extend({
        el : "#platform-table",
        template : $("#platform-table-template").html(),
        initialize : function() {
            this.render();
        },
        getData : function() {
            return this.collection.getPlatformsSummary();
        },
        render : function() {
            var data = this.getData();
            this.$el.html(_.template(this.template, {
                "data" : data
            }));
        }
    });

    var PlatformIndexView = Backbone.View.extend({
        el : "#platform_pie",
        initialize : function() {
            this.render();
        },
        getPieData : function() {
            var that = this;
            return this.collection.platforms.map(function(platform){
                return {
                    label : platform.get('code'),
                    data : that.collection.getSummaryByPlatform(platform.get('id'))['total']
                };
            });
        },
        render : function() {
            var platformData = this.getPieData();
            var platformPie = new IndexPie();
            platformPie.render(platformData, this.$el);
            this.platformTable = new TablePlatformView({
                collection : this.collection
            });
        }
    });

    var TableProjectView = TablePlatformView.extend({
        el : "#project-table",
        template : $("#project-table-template").html(),
        getData : function() {
            return this.collection.getProjectsSummary();
        }
    });

    var ProjectIndexView = PlatformIndexView.extend({
        el : "#project_pie",
        getPieData : function() {
            var that = this;
            return this.collection.projects.map(function(project){
                return {
                    label : project.get('code'),
                    data : that.collection.getSummaryByProject(project.get('id')).total
                };
            });
        },
        render : function() {
            var projectData = this.getPieData();
            var projectPie = new IndexPie();
            projectPie.render(projectData, this.$el);
            this.projectTable = new TableProjectView({
                collection : this.collection
            });
        }
    });

})();