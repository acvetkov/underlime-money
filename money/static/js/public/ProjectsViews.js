/*global Backbone, _*/
var ProjectView;
(function(){
    'use strict';
    ProjectView = Backbone.View.extend({
        el : "#site-content",
        template : $("#project-page-template").html(),
        render : function() {
            this.$el.html(_.template(this.template));
            this.chartView = new ChartView();
        }
    });

    var ChartView = Backbone.View.extend({
        el : "#line_chart",
        initialize : function() {
            var d1 = [],
                d2 = [];

            for (var i = 0; i < 50; i += 0.1) {
                d1.push([i, Math.sin(i)]);
                d2.push([i, Math.sin(i) * 2]);
            }

            var options = {
                colors : ["rgba(66,139,202,0.8)", "rgba(151,187,205,0.8)"],
                series: {
                    lines: { show: true },
                    points: { show: false }
                },
                grid : {
                    show: true,
                    color: "#ccc",
                    borderWidth: 1,
                    borderColor: "#f5f5f5"
                }
            };

            var data = [{
                'label' : "TK",
                'data' : d1
            }, {
                'label' : "SD",
                'data' : d2
            }];

            $.plot(this.el, data, options);
        }
    });

})();
