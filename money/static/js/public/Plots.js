/*global Backbone, _*/
var IndexPie, LineChart;
(function() {
    'use strict';
    IndexPie = Backbone.View.extend({
        render : function(data, $el) {
            this.data = data;

            function format(label, series) {
                return _.template($("#plot-formatter-label").html(), {label : label, percent : Math.round(series.percent)})
            }
            var options = {
                colors : ColorsHelper.colors,
                series : {
                    pie : {
                        show: true,
                        radius: 1,
                        stroke : {
                            color : "rgba(151,187,205,0.5)",
                            width : 1
                        },
                        label : {
                            show : true,
                            background : {
                                color : "#000",
                                opacity : 0.5
                            },
                            formatter : format
                        }
                    }
                },
                legend : {
                    show: true
                }
            };
            $.plot($el, this.data, options);
        }
    });

    LineChart = Backbone.View.extend({
        render : function(data, $el) {
            $.plot($el, data);
        }
    });

    var ColorsHelper = {
        colors : ["rgba(151,187,205,0.5)", "rgba(220,220,220,0.5)", "rgba(66, 139, 202, 0.5)", "#ddd"],
        getColor : function(index) {
            if (this.colors[index] !== undefined) {
                return this.colors[index];
            }
            return this.colors[0];
        }
    };

})();

