/* global Backbone, _ */

var IndexPaymentCollection;

(function(){
    'use strict';
    /**
     * Коллекция платформ
     */
    var ProjectCollection = Backbone.Collection.extend({
        initialize : function() {
            this.model = Platform;
            if (typeof window._projects_ !== "undefined") {
                this.reset(window._projects_);
            }
        }
    });

    /**
     * Коллекция проектов
     * @type {*}
     */
    var PlatformCollection = Backbone.Collection.extend({
        initialize : function() {
            this.model = Project;
            if (window._platforms_) {
                this.reset(window._platforms_);
            }
        }
    });

    /**
     * Коллекция платежей
     */
    IndexPaymentCollection = Backbone.Collection.extend({
        initialize : function() {
            this.model = IndexPayment;
            this.projects = new ProjectCollection();
            this.platforms = new PlatformCollection();
            if (typeof window._index_collection_ !== "undefined") {
                this.reset(window._index_collection_);
            }
        },
        getPlatformsSummary : function() {
            var self = this;
            var index = 0;
            return this.platforms.map(function(platform) {
                var pays = self.where({
                    "platform_id" : platform.get('id')
                });
                index += 1;
                return CollectionHelper.getSummaryTotal(pays, platform, index);
            });
        },
        getProjectsSummary : function() {
            var self = this;
            var index = 0;
            return this.projects.map(function(project) {
                var pays = self.where({
                    "project_id" : project.get('id')
                });
                index += 1;
                return CollectionHelper.getSummaryTotal(pays, project, index);
            });
        },
        getSummaryByProject : function(project_id) {
            var pays = this.where({"project_id" : project_id});
            return CollectionHelper.getSummaryTotal(pays);
        },
        getSummaryByPlatform : function(platform_id) {
            var pays = this.where({"platform_id" : platform_id});
            return CollectionHelper.getSummaryTotal(pays);
        }
    });

    var CollectionHelper = {
        getTotalSum : function(pays) {
          return _.reduce(pays, function(sum, item) {
              return sum + item.get('total_sum');
          }, 0);
        },
        getAvSum : function(pays) {
            var total = _.reduce(pays, function(sum, item) {
                return sum + item.get('avg_sum');
            }, 0);
            return Math.round(total / pays.length);
        },
        getCount : function(pays) {
            return _.reduce(pays, function(sum, item) {
                return sum + item.get('total_count');
            }, 0);
        },
        getSummaryTotal : function(pays, model, index) {
            return {
                "number" : index,
                "name" : model ? model.get('name') : "",
                "model" : model,
                "total" : this.getTotalSum(pays),
                "avg" : this.getAvSum(pays),
                "count" : this.getCount(pays)
            };
        }
    };

    var IndexPayment = Backbone.Model.extend({
        defaults : {
            project_id : 0,
            platform_id : "",
            total_sum : 0,
            total_count : 0,
            avg_sum : 0
        }
    });

    var Platform = Backbone.Model.extend({
        defaults : {
            id : 0,
            name : "",
            code : ""
        }
    });

    var Project = Backbone.Model.extend({
        defaults : {
            id : 0,
            name : "",
            code : ""
        }
    });
})();