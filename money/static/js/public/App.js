/* global IndexPaymentCollection, IndexView, ProjectView, NavigationMenuView, Backbone, Routes*/
$(function(){
    'use strict';
    var App = {
        $container : $("#site-content"),
        collection : new IndexPaymentCollection(),
        views : {},
        routes : new Routes(),
        clear : function() {
            this.$container.empty();
        },
        init : function() {
            var that = this;
            this.views = {
                indexView : new IndexView({
                    collection: this.collection
                }),
                projectsView : new ProjectView(),
                navigation : new NavigationMenuView(this.routes)
            };
            this.routes.on('route:index', function(){
                console.log("index");
                that.clear();
                that.views.indexView.render();
            });
            this.routes.on('route:projects', function(){
                console.log("projects");
                that.clear();
                that.views.projectsView.render();
            });
            this.routes.on('route:platforms', function(){
                console.log("platforms");
                that.clear();
            });
        }
    };

    App.init();
    Backbone.history.start();
});