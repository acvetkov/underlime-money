/* global Backbone*/
var NavigationMenuView;

(function(){
    'use strict';
    NavigationMenuView = Backbone.View.extend({
        el : "#site-navigation",
        events : {
            "click li" : function(event) {
                var $target = $(event.currentTarget);
                if (!$target.hasClass('active')) {
                    this.clear();
                    $target.addClass("active");
                }
            }
        },
        constructor : function(routes) {
            NavigationMenuView.__super__.constructor.apply(this, arguments);
            this.routes = routes;
            this.watchRoutes();
        },
        clear : function() {
            this.$el.find("li").removeClass('active');
        },
        watchRoutes : function() {
            var that = this;
            this.routes.on('route', function(){
                var alias = this.routes[Backbone.history.fragment];
                that.updateMenu(alias);
            });
        },
        updateMenu : function(routeName) {
            this.clear();
            var $target = $( "[data-route='" + routeName + "']", this.el);
            $target.addClass('active');
            $('title').html($target.attr('data-title'));
        }
    });
})();
