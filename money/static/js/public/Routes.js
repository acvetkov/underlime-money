/* global Backbone */
var Routes;
(function(){
    'use strict';
    Routes = Backbone.Router.extend({
        routes : {
            'index/' : "index",
            'platforms/' : 'platforms',
            'projects/' : 'projects',
            '*path':  'defaultRoute' // default route
        },
        defaultRoute : function() {
            this.navigate('index/', {trigger : true});
        }
    });
})();
