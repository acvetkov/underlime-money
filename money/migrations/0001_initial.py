# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Platforms'
        db.create_table(u'money_platforms', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=8)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('pay_k', self.gf('django.db.models.fields.FloatField')(default=1)),
        ))
        db.send_create_signal(u'money', ['Platforms'])

        # Adding model 'Projects'
        db.create_table(u'money_projects', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=8)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'money', ['Projects'])

        # Adding M2M table for field platforms on 'Projects'
        db.create_table(u'money_projects_platforms', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('projects', models.ForeignKey(orm[u'money.projects'], null=False)),
            ('platforms', models.ForeignKey(orm[u'money.platforms'], null=False))
        ))
        db.create_unique(u'money_projects_platforms', ['projects_id', 'platforms_id'])

        # Adding model 'Payment'
        db.create_table(u'money_payment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sum', self.gf('django.db.models.fields.FloatField')()),
            ('platform', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['money.Platforms'])),
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['money.Projects'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 2, 6, 0, 0))),
        ))
        db.send_create_signal(u'money', ['Payment'])


    def backwards(self, orm):
        # Deleting model 'Platforms'
        db.delete_table(u'money_platforms')

        # Deleting model 'Projects'
        db.delete_table(u'money_projects')

        # Removing M2M table for field platforms on 'Projects'
        db.delete_table('money_projects_platforms')

        # Deleting model 'Payment'
        db.delete_table(u'money_payment')


    models = {
        u'money.payment': {
            'Meta': {'object_name': 'Payment'},
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 6, 0, 0)'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['money.Platforms']"}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['money.Projects']"}),
            'sum': ('django.db.models.fields.FloatField', [], {})
        },
        u'money.platforms': {
            'Meta': {'object_name': 'Platforms'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '8'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pay_k': ('django.db.models.fields.FloatField', [], {'default': '1'})
        },
        u'money.projects': {
            'Meta': {'object_name': 'Projects'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '8'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'platforms': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['money.Platforms']", 'symmetrical': 'False'})
        }
    }

    complete_apps = ['money']