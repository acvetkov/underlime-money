# -*- coding: utf-8 -*-
import datetime
import random
from south.db import db
from south.v2 import DataMigration
from django.db import models


class Migration(DataMigration):
    def forwards(self, orm):
        orm.Platforms.objects.bulk_create([
            orm.Platforms(code="OK", name=u'Одноклассники', pay_k=1),
            orm.Platforms(code="VK", name=u'Вконтакте', pay_k=0.5),
            orm.Platforms(code="FB", name=u'Facebook', pay_k=0.8),
            orm.Platforms(code="MM", name=u'Мой мир', pay_k=1),
        ])

        orm.Projects.objects.bulk_create([
            orm.Projects(code="TK", name=u'Томато Комбат'),
            orm.Projects(code="SD", name=u'Космо Дайвер')
        ])

        def get_platform(orm):
            rand = random.randint(0, 100)
            if 0 <= rand < 40:
                return orm['money.Platforms'].objects.all()[0]
            if 40 <= rand < 70:
                return orm['money.Platforms'].objects.all()[1]
            if 70 <= rand < 90:
                return orm['money.Platforms'].objects.all()[2]
            return orm['money.Platforms'].objects.all()[3]

        def get_project(orm):
            rand = random.randint(0, 100)
            if 0 <= rand < 80:
                return orm['money.Projects'].objects.all()[0]
            return orm['money.Projects'].objects.all()[1]

        pay_date = datetime.datetime(2013, 6, 1)
        for day in range(1, 273):
            for pay in range(1, random.randint(50, 500)):
                payment = orm.Payment()
                payment.sum = random.randint(1, 100)
                payment.platform = get_platform(orm)
                payment.project = get_project(orm)
                payment.date = pay_date
                payment.save()

            pay_date += datetime.timedelta(days=1)


    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        u'money.payment': {
            'Meta': {'object_name': 'Payment'},
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 6, 0, 0)'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['money.Platforms']"}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['money.Projects']"}),
            'sum': ('django.db.models.fields.FloatField', [], {})
        },
        u'money.platforms': {
            'Meta': {'object_name': 'Platforms'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '8'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pay_k': ('django.db.models.fields.FloatField', [], {'default': '1'})
        },
        u'money.projects': {
            'Meta': {'object_name': 'Projects'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '8'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'platforms': ('django.db.models.fields.related.ManyToManyField', [],
                          {'to': u"orm['money.Platforms']", 'symmetrical': 'False'})
        }
    }

    complete_apps = ['money']
    symmetrical = True
