module.exports(function(grunt) {
    'use strict';
    grunt.initConfig({
        pkg : grunt.file.readJSON('package.json'),
        jshint : {
            options : {
                ignores : ['src/js/lib/**/*.js']
            },
            '<%=pkg.name%>' : {
                src : ['src/js/**/*.js']
            }
        },
        concat : {
            dist: {
                src : ['src/js/**/*.js'],
                dest : "build/js/build.js"
            }
        },
        uglify : {
            options: {
                stripBanners : true,
                banner : "/*<%=pkg.name%> -v<%=pkg.version%> - <%=grunt.template.today('dd-mm-yyyy')%>*/\n"
            },
            build : {
                src : "build/js/build-<%=pkg.version%>.js",
                dest : "build/js/build-<%=pkg.version%>.min.js"
            }
        },
        cssmin : {
            with_banner : {
                options : {
                    banner : "/*<%=pkg.name%> -v<%=pkg.version%> - <%=grunt.template.today('dd-mm-yyyy')%>*/\n"
                },
                files : {
                    'build/css/build-<%=pkg.version%>.min.css' : ['src/css/**/*.css']
                }
            }
        },
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: {
                    'src/index.html': 'src/html/index.html'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');

    grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'cssmin']);
    grunt.registerTask('debug', ['']);

});
